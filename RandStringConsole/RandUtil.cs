﻿//  Author:
//       Zack Casey <dotfoxcomp@outlook.com>
//
//  Copyright (c) 2015 Zack Casey
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
using System;
using System.IO;

namespace RandUtilLib
{
	// <summary>
	// RandUtil is based on a tutorial from http://www.dotnetperls.com/random-string.
	// The core functionaility of RandString method. The RandOutput was from the example
	// but in the Main() method before becoming it's own.
	// </summary>
	public class RandUtil
	{
		
		// </summary>
		//	Gets a random name using System.IO's
		//	"GetRandomFileName." The method uses
		//	RNGCryptoServiceProvider for
		//	as a seed but is limited to 11 chars.
		//	</summary>
		public static string RandomString ()
		{
			string path = Path.GetRandomFileName ();
			path = path.Replace (".", "");
			return path;
		}


		//	</summary>
		//	Hard-coded random output. Less flexiable but
		//	offers an quick solution.
		//	</summary>
		public static void RandOutput ()
		{
			Console.WriteLine (RandUtil.RandomString ());
			Console.WriteLine (RandUtil.RandomString ());
			Console.WriteLine (RandUtil.RandomString ());
			Console.WriteLine (RandUtil.RandomString ());
			Console.WriteLine (RandUtil.RandomString ());
		}
	}
}

