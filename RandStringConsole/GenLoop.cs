﻿//  Author:
//       Zack Casey <dotfoxcomp@outlook.com>
//
//  Copyright (c) 2015 Zack Casey
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
using System;
using System.IO;
using System.Windows.Forms;
using RandUtilLib;

namespace RandStringConsole
{
	public class GenLoop
	{
		#region String Generator

		// <summary>
		// The GenLoop is where the bulk of the app takes place. It starts
		// by creating five random strings followed by a promt, asking the
		// the user if they wish to copy, create a new batch, or quite.
		//
		// Quiting breaks the loop, forcing the application to exit to desktop
		// while the other two goes to the QLoop (Quite Loop).
		// </summary>

		public static void MainLoop ()
		{
			int key = 0;

			var copy = RandUtil.RandomString () + "\n" +
			           RandUtil.RandomString () + "\n" +
			           RandUtil.RandomString () + "\n" +
			           RandUtil.RandomString () + "\n" +
			           RandUtil.RandomString () + "\n";

			Console.WriteLine (copy);

			while (true) {
				Console.WriteLine ("Do you wish to copy? Y/N/Q");
				Console.Write ("Y: Copies, and asks to quit.\n"
				+ "N: Creates new string.\n"
				+ "Q: Quites to desktop.\n");
				key = (int)Console.ReadKey ().Key;

				// Copies the random string and begins the loop
				if (key == 89) { // Y
					Clipboard.SetText (copy);
					QLoop ();
				} else if (key == 78) { // N
					Console.Clear ();
					MainLoop ();
				} else if (key == 81) { // Q
					break; // Breaks the infinite loop
				}
				Console.WriteLine (Environment.NewLine);
			}
		}

		// <summary>
		// The QLoop (Quite Loop) asks the user if they qish to continue
		// If the user chooses "Y", it opens GenLoop, otherwise it
		// breaks the loop and closes the application.
		// </summary>
		private static void QLoop ()
		{
			int key = 0;

			while (true) {
				Console.Clear ();
				Console.WriteLine ("Do you wish to continue? Y/N\n" +
				"If you continue, a new string will be generated.");

				key = (int)Console.ReadKey ().Key;

				if (key == 78) {
					break;
				} else if (key == 89) {
					Console.Clear ();
					MainLoop ();
				}
			}
		}

		#endregion
	}
}

